import kotlin.system.measureTimeMillis

enum class Direction {
    FORWARD, TOP, BOTTOM, LEFT, RIGHT
}

class Cube(val direction: Direction, val prev: Cube?) {
    private enum class AbsDirection {
        XP, XM, YP, YM, ZP, ZM
    }

    private val absDirection: AbsDirection = if (prev == null) AbsDirection.XP else
        when (prev.direction) {
            Direction.FORWARD -> prev.absDirection
            Direction.LEFT ->
                when (prev.absDirection) {
                    AbsDirection.XM -> AbsDirection.YM
                    AbsDirection.YM -> AbsDirection.XP
                    AbsDirection.XP -> AbsDirection.YP
                    AbsDirection.YP -> AbsDirection.XM

                    AbsDirection.ZM -> AbsDirection.YM
                    AbsDirection.ZP -> AbsDirection.YP
                }
            Direction.RIGHT ->
                when (prev.absDirection) {
                    AbsDirection.XM -> AbsDirection.YP
                    AbsDirection.YP -> AbsDirection.XP
                    AbsDirection.XP -> AbsDirection.YM
                    AbsDirection.YM -> AbsDirection.XM

                    AbsDirection.ZM -> AbsDirection.YP
                    AbsDirection.ZP -> AbsDirection.YM
                }
            Direction.BOTTOM ->
                when (prev.absDirection) {
                    AbsDirection.XM -> AbsDirection.ZM
                    AbsDirection.ZM -> AbsDirection.XP
                    AbsDirection.XP -> AbsDirection.ZP
                    AbsDirection.ZP -> AbsDirection.XM

                    AbsDirection.YM -> AbsDirection.ZM
                    AbsDirection.YP -> AbsDirection.ZP
                }
            Direction.TOP ->
                when (prev.absDirection) {
                    AbsDirection.XM -> AbsDirection.ZP
                    AbsDirection.ZP -> AbsDirection.XP
                    AbsDirection.XP -> AbsDirection.ZM
                    AbsDirection.ZM -> AbsDirection.XM

                    AbsDirection.YM -> AbsDirection.ZP
                    AbsDirection.YP -> AbsDirection.ZM
                }
        }

    private val x: Int = if (prev == null) 0 else
        when (absDirection) {
            AbsDirection.XM -> prev.x - 1
            AbsDirection.XP -> prev.x + 1
            else -> prev.x
        }
    private val y: Int = if (prev == null) 0 else
        when (absDirection) {
            AbsDirection.YM -> prev.y - 1
            AbsDirection.YP -> prev.y + 1
            else -> prev.y
        }
    private val z: Int = if (prev == null) 0 else
        when (absDirection) {
            AbsDirection.ZM -> prev.z - 1
            AbsDirection.ZP -> prev.z + 1
            else -> prev.z
        }

    override fun toString(): String {
        return if (prev != null) prev.toString() + " -> " + x.toString() + ',' + y.toString() + ',' + z.toString()
        else x.toString() + ',' + y.toString() + ',' + z.toString()
    }

    private val maxX: Int = if (prev != null && x < prev.maxX) prev.maxX else x
    private val minX: Int = if (prev != null && x > prev.minX) prev.minX else x
    private val maxY: Int = if (prev != null && y < prev.maxY) prev.maxY else y
    private val minY: Int = if (prev != null && y > prev.minY) prev.minY else y
    private val maxZ: Int = if (prev != null && z < prev.maxZ) prev.maxZ else z
    private val minZ: Int = if (prev != null && z > prev.minZ) prev.minZ else z

    val checkDimensions: Boolean = (maxX - minX < 3) && (maxY - minY < 3) && (maxZ - minZ < 3)

    val checkPosition: Boolean by lazy {
        fun comp(cube: Cube, x: Int, y: Int, z: Int): Boolean {
            return if (cube.prev != null) comp(cube.prev, x, y, z) || ((cube.x == x) && (cube.y == y) && (cube.z == z)) else
                (cube.x == x) && (cube.y == y) && (cube.z == z)
        }
        if (prev != null) comp(prev, x, y, z) else false
    }

    val size: Int = 1 + if (prev != null) prev.size else 0
}


fun main(args: Array<String>) {
    val fwd = arrayOf(true, true, false, // 1-3
            true, false, //4-5
            true, false, //6-7
            true, false, //8-9
            false, //10
            false, //11
            false, //12
            true, false, //13-14
            true, false, //15-16
            false, //17
            false, //18
            true, false, //19-20
            false, //21
            true, false, //22-23
            false, //24
            false, //25
            true, true) // 26-27

    fun propagateCube(cube: Cube, Acc: Set<Cube?>): Set<Cube?> {
        if (cube.size == 27) return Acc + cube else {
            if (fwd[cube.size]) {
                val c1 = Cube(Direction.FORWARD, cube)
                if (c1.checkDimensions && !c1.checkPosition)
                    return Acc + propagateCube(c1, Acc)
                else return Acc
            } else {
                val c1 = Cube(Direction.LEFT, cube)
                val c2 = Cube(Direction.RIGHT, cube)
                val c3 = Cube(Direction.BOTTOM, cube)
                val c4 = Cube(Direction.TOP, cube)

                return Acc + (if (c1.checkDimensions && !c1.checkPosition) propagateCube(c1, Acc) else setOf(null)) +
                        (if (c2.checkDimensions && !c2.checkPosition) propagateCube(c2, Acc) else setOf(null)) +
                        (if (c3.checkDimensions && !c3.checkPosition) propagateCube(c3, Acc) else setOf(null)) +
                        (if (c4.checkDimensions && !c4.checkPosition) propagateCube(c4, Acc) else setOf(null))
            }
        }
    }

    val solution = propagateCube(Cube(Direction.FORWARD, null), setOf(null))
    println(solution.last())
}